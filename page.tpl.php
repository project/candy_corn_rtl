<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

  <div id="banner">
    <div class="top_links clearfix" id="topnav">
	<?php if (count($secondary_links)) : ?>
	<ul id="secondary">
	<?php foreach ($secondary_links as $link): ?>
	<li>&nbsp;<?php print $link?>&nbsp;</li>
	<?php endforeach; ?>
	</ul>
	<?php endif; ?>
    </div>
    <?php if ($logo) { ?><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
    <div class="page_title"><?php if ($site_name) { ?><a id="page_title" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a><?php } ?><br />
    <?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?></div>
  </div>


	
  <?php if ($sidebar_right) { ?>
    <div class="rightcontent">
      <img alt="bg image" src="<?php echo base_path().drupal_get_path('theme', 'candy_corn_rtl'); ?>/images/right_bg_top.gif" />
       <?php if (count($primary_links)) : ?>
	   <ul id="primary">
	     <?php foreach ($primary_links as $link): ?>
	       <li><?php print $link?></li>
	     <?php endforeach; ?>
	   </ul>
       <?php endif; ?>

	  <div class="right_news">
      <p>&nbsp;</p>
      <?php print $sidebar_right ?>
      </div>
    </div>
  <?php } ?>

  <div id="centercontent">
    <div><?php print $header ?></div>
    <div class="pumpkin"></div><div id="news_title_blk"><?php print $title ?></div>
	<div class="tabs"><?php print $tabs ?></div>
        <?php if ($mission != ""): ?>
          <div id="mission"><?php print $mission ?></div>
        <?php endif; ?>
        
    <?php print $help ?>
    <?php print $messages ?>
    <?php print $content; ?>
    <br />
  </div>

  <?php if ($sidebar_left) { ?>
    <div id="leftcontent">
      <div class="left_news">
        <?php print $search_box ?>
		<br />
        <?php print $sidebar_left ?>
      </div>
    </div>
  <?php } ?>
	
  <div id="footer">
    <p><?php print $footer_message ?></p>
  </div>
  <?php print $closure ?>
</body>
</html>
